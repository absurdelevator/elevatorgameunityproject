﻿using UnityEngine;
using System.Collections;
using System.Linq;
public class SymbolPlayer : MonoBehaviour {
	AudioSource source;
	AudioClip[] clips;

	void Start () {
		source = (new GameObject ()).AddComponent<AudioSource> ();
		source.loop = false;
		//source.spatialBlend = 0; set to all 2D
		var cp = Resources.LoadAll ("symbols"); //load all sounds in the Resources/symbols folder
		//for some reason it's not letting me upcast into AudioClip[] directly so I do it as an extra step.
		clips = new AudioClip[cp.Length]; 
		for (int i = 0; i < cp.Length; i++)
			clips [i] = cp [i] as AudioClip;
			
		//StartCoroutine(PlaySymbols (new string[]{ "Peel", "Space", "Cigarette" }));
		//foreach (var e in clips)
			//Debug.Log (e.name);
	}

	public IEnumerator PlaySymbols(string[] symbols)
	{
		print ("playing sounds");
		print (symbols.Length);
		foreach (var e in symbols) {
			var playme = clips.FirstOrDefault (f => f.name == e);
			yield return StartCoroutine(PlaySound(playme));
		}
	}

	IEnumerator PlaySound(AudioClip clip)
	{
		print (clip.name);
		//Debug.Log ("playing sound");
		source.clip = clip;
		source.Play ();
		//while (source.isPlaying) yield return null;
		yield return new WaitForSeconds(.6f);
	}
}
