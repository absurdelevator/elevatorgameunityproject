﻿using UnityEngine;
using System.Collections;

public class RotateAboutAxis : MonoBehaviour {

	public Vector3 axis;
	public float speed; //in degrees per second

	void Update () {
		transform.Rotate(axis, speed * Time.deltaTime,Space.Self);
	}
}
