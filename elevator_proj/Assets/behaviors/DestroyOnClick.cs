﻿using UnityEngine;
using System.Collections;

public class DestroyOnClick : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			GameObject.Destroy (gameObject);
			GameObject.Find ("Main Camera").GetComponent<boarding>().gameStarted = true;
		}
	}
}
