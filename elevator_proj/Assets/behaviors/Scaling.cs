﻿using UnityEngine;
using System.Collections;

public class Scaling : MonoBehaviour {
	public float speed;
	public float amplitude;
	public float offset = 0;
	public Vector3 axis = new Vector3(1,1,1);

	Vector3 startScale;

	void Start () {
		startScale = transform.localScale;
	}

	// Update is called once per frame
	void Update () {
		transform.localScale = startScale + axis * Mathf.Sin (offset + Time.time * speed) * amplitude;

	}
}
