﻿using UnityEngine;
using System.Collections;

public class RotateJiggle : MonoBehaviour {
	public float speed;
	public float amplitude;
	public float offset = 0;
	public Vector3 axis;

	Quaternion start;

	void Start () {
		start = transform.localRotation;
	}

	// Update is called once per frame
	void Update () {
		transform.localRotation = start * Quaternion.AngleAxis(Mathf.Sin (offset + Time.time * speed) * amplitude, axis);

	}
}
