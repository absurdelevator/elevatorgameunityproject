﻿using UnityEngine;
using System.Collections;

public class Bobbing : MonoBehaviour {
	public float speed;
	public float amplitude;
	public float offset = 0;
	public Vector3 axis;
	Vector3 start;

	public bool useLocal = false;

	void Start () {
		if (useLocal)
			start = transform.localPosition;
		else
			start = transform.position;

		axis = axis.normalized;
	}
	
	// Update is called once per frame
	void Update () {
		if (useLocal) {
			transform.localPosition = start + axis * Mathf.Sin (offset + Time.time * speed) * amplitude;
		} else {
			transform.position = start + axis * Mathf.Sin (offset + Time.time * speed) * amplitude;
		}
	}
}
