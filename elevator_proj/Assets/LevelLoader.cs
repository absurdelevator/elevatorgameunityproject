﻿using UnityEngine;
using System.Collections.Generic;

public class LevelLoader : MonoBehaviour {

	public GameObject[] levels;
	GameObject elevator;
	GameObject lobby;

	List<GameObject> loadedLevels = new List<GameObject>();

	void Start () {
		elevator = GameObject.Find ("elevator");
		levels = GameObject.FindGameObjectsWithTag ("FloorLayer");
		lobby = GameObject.Find ("Lobby");
	}

	void Update () {
	
	}
	public void UnloadLevel()
	{
		foreach (var e in loadedLevels)
			GameObject.Destroy (e);
		loadedLevels.Clear ();
	}

	public void LoadLobby()
	{
		UnloadLevel ();
		GameObject newLevel = (GameObject)GameObject.Instantiate (lobby,new Vector3(.85f, .83f, -5.5f),Quaternion.identity);
		loadedLevels.Add (newLevel);
	}

	public void LoadLevel(List<string> aLevels)
	{
		UnloadLevel ();
		foreach (var e in aLevels) {
			foreach (var f in levels) {
				if (f.name == e) {
					GameObject newLevel = (GameObject)GameObject.Instantiate (f,elevator.transform.position,Quaternion.identity);
					loadedLevels.Add (newLevel);
					break;
				}
			}
				
		}
	}
}
