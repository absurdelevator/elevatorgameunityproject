﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameCharacter {
	public string Name { get; set; }
	public List<string> enterDialog { get; set; }
	public List<string> favKeys { get; set; }
	public string Success { get; set; }
	public string Fail { get; set; }
	public GameCharacter()
	{
	}
}

public class Ingrediant {
	public string Name { get; set; }
	public bool HasSound { get; set; }
	public AudioClip AClip { get; set; }
	public Ingrediant()
	{
	}
}

public class boarding : MonoBehaviour {

	LevelLoader loader;
	SymbolPlayer sp;

	public bool gameStarted = false;

	public AudioClip lobbyClip;
	public AudioClip bananaClip;
	public AudioClip fruitClip;
	public AudioClip lemonClip;
	public AudioClip spaceClip;
	public AudioClip loveClip;
	public AudioClip candyClip;
	public AudioClip catClip;

	List<GameCharacter> allChars = new List<GameCharacter>()
	{
		new GameCharacter(){ Name = "Imma Banana", enterDialog = new List<string>(){
				"Operator: What will it be today?",
				"Imma Banana: I shave myself everyday to hide my spots. I shave in the morning. But also at night. \n" +
				"No one wants bananas with bananage spots.",
				"Imma Banana: I’m so embarrassed to show up like this in front of the other fruits." +
				"How will Mz. Lemon Squeezy ever like me now?\n" + 
				"Take me to where the fruits are made into salads. Today will be my day."
			}, favKeys = new List<string>() {
				"Lemon", "Peel", "Fruit"
			}, Success = "Imma Banana: Yasss fruit party!! Luckily I shaved. So many fruit!",
			Fail = "Imma Banana: What is this place? This fruitless abyss has got me weirdly questioning who I am."
		},
		new GameCharacter(){ Name = "Derda Flip", enterDialog = new List<string>(){
				"Operator: What will it be today?",
				"Derda Flip:  breakfast, roll, lick, smoke, lunch, roll, lick, smoke, break, roll, lick, smoke, " +
				"dinner, roll, lick, smoke, drink, roll, lick, smoke, dance, roll, lick, smoke, breakfast, roll, " +
				"lick, smoke, lunch, roll, lick, smoke,  break, roll, lick, smoke, dinner, roll, lick, smoke, drink, " +
				"roll lick, smoke, dance, roll, lick, smoke, breakfast, roll, lick, smoke, lunch, roll, lick, smoke, " +
				"break, roll, lick, smoke, dinner, roll, lick, smoke, drink, roll lick, smoke, dance, roll lick, smoke.",
				"Operator: I'm sorry, what was that?",
				"Derda Flip: breakfast, roll, lick, smoke, lunch, roll, lick, smoke, break, roll, lick, smoke, dinner, " +
				"roll, lick, smoke, drink, roll, lick, smoke, dance, roll, lick, smoke, breakfast, roll, lick, smoke, lunch, " +
				"roll, lick, smoke,  break, roll, lick, smoke, dinner, roll, lick, smoke, drink, roll lick, smoke, dance. Smoky."
			}, favKeys = new List<string>() {
				"Coffee", "Cigarette", "Beer"
			}, Success = "Derda Flip: Oh, smokylicky. Thanks.\nRoll is life, lick is life, smoke is life.",
			Fail = "Derda Flip: No roll, no lick, no smoke here...\nWhat new roll? what new lick? what new smoke? ..."},
		new GameCharacter(){ Name = "Mz. Lemon Squeezy", enterDialog = new List<string>(){
				"Operator: What will it be today?",
				"Mz. Lemon Squeezy: Where are we dear? My head it so heavy.  Have I swimmed too much? " +
				"But now I can eat those fabulous 300cal donuts with my favorite cup of tea.",
				"Mz. Lemon Squeezy: With lemon. Yes with my own fresh squeezed lemon juice.\n" +
				"Yes, that's it, darling!"
			}, favKeys = new List<string>() {
				"Donut", "Lemon", "Tea"
			}, Success = "Mz. Lemon Squeezy: Ooh delicious! I might have to swim again after I’m done here.",
			Fail = "Mz. Lemon Squeezy: I'm really craving those tea and donuts. I was hoping you'd help me cheat. Oh well, exploring burns calories!"},
		new GameCharacter(){ Name = "Mac Blubson", enterDialog = new List<string>(){
				"Operator: What will it be today?",
				"Mac Blubson: bblblblblbl bubblyfloor. Wh wh where do do you you came from?\n" +
				"Mac Blubson: Oh I know you come from! I saw on Faceblblbl that Imma Banana is " +
				"blblblblb with someone from Lemonheadland.",
				"Mac Blubson: Blblbl you know my friendblbllblbl." +
				"Oh wait blblbll did you see my Fbllaacebllllll post about the cosmoblblbcat?"
			}, favKeys = new List<string>() {
				"Facebook", "Space", "Cat"
			}, Success = "Mac Blubson: Bllbll please add me blllbllll. Catstars are blblllblleverybllllthiing.",
			Fail = "Mac Blubson: Blllll where are the blllllcats????" 
		},
		new GameCharacter(){ Name = "Skell E. Ton", enterDialog = new List<string>(){
				"Operator: What will it be today?",
				"Skell E. Ton: I should shake your hand. Or give you one, two or three kisses? " +
				"I’m not really soft I know...but...I don’t really know, every floor people change rules and " +
				"I don’t know what to do when introducing myself...What do you prefer?",
				"Operator: Where are you trying to go?\n", 		
				"Skell E. Ton: Please, my friend. Shake my hand. Let's get drinks.",
				"Operator: I am an elevator operator. I will take you to a floor.\n",
				"Skell E. Ton: My friend. Porque?"
			}, favKeys = new List<string>() {
				"Kiss", "Hand", "Beer"
			}, Success = "Skell E. Ton: My friend let me give you a kiss. You are the best. You and me. Forever. Never forget.",
			Fail = "Skell E. Ton: First you don’t want to shake my hand and now you don’t want to take me to where the love is at? " +
				"Come on my friend. Why you do this to me?"
		}
	};

	Dictionary<string, Ingrediant> allIngrediants = new Dictionary<string, Ingrediant>() {
		{ "u", new Ingrediant {Name="Candy", HasSound=true} },
		{ "h", new Ingrediant {Name="Cat", HasSound=true} },
		{ "v", new Ingrediant {Name="Forest", HasSound=false} },
		{ "r", new Ingrediant {Name="Lemon", HasSound=true} },
		{ "f", new Ingrediant {Name="Facebook", HasSound=false} },
		{ "n", new Ingrediant {Name="Love", HasSound=true} },
		{ "i", new Ingrediant {Name="Cigarette", HasSound=false} }, //NEED ICON
		{ "y", new Ingrediant {Name="Fruit", HasSound=true} },
		{ "t", new Ingrediant {Name="Coffee", HasSound=false} }, //NEED LEVEL
		{ "k", new Ingrediant {Name="Beer", HasSound=false} }, //NEED ICON
		{ "b", new Ingrediant {Name="Space", HasSound=true} },
		{ "m", new Ingrediant {Name="Hand", HasSound=false} },
		{ "g", new Ingrediant {Name="Peel", HasSound=false} }, //NEEDALL
		{ "j", new Ingrediant {Name="Tea", HasSound=false} } //NEEDALL
	}; 
	
	public float entranceDelay = 1.5f;
	public float elevatorSpeed = .05f;
	public GameObject doorL;
	public GameObject doorR;
	public GameObject cam;
	public GameObject panel;
	public GameObject choose3;
	public int characterStatus = 0;
	// 0 = not with character, 1 = character entering, 2 = ready to choose, 3 = moving up, 4 = exiting, 0?

	Vector3 doorLClosed, doorRClosed;

	public int currCharacter = 0;
	private AudioSource currCharSource;
	private int currLine = 0;
	public List<string> currLines = new List<string>();
	public List<string> currIngrediants = new List<string>();
	private List<string> chosenIgs = new List<string> ();

	private GameObject[] icons;
	private GameObject[] chars;

	public GameObject sBox;
	public Text sText;
	public GameObject credits;

	public AudioClip eOpen;
	public AudioClip eClose;
	public AudioClip eUp;
	public AudioClip eDown;
	public AudioClip eDing;
	public AudioSource eSource; //Doors
	public AudioSource rSource; //Voice
	public AudioSource lSource; //Level

	// Use this for initialization
	void Start () {
		doorL = GameObject.Find ("DoorL");
		doorR = GameObject.Find ("DoorR");
		doorLClosed = doorL.transform.position;
		doorRClosed = doorR.transform.position;
		cam = GameObject.Find ("Main Camera");
		panel = GameObject.Find ("panel");
		icons = GameObject.FindGameObjectsWithTag ("Icon");
		choose3 = GameObject.Find ("ChoosePrompt");

		loader = GameObject.Find ("elevator").GetComponent<LevelLoader> ();
		sp = cam.GetComponent<SymbolPlayer> ();
		eSource = GameObject.Find ("DoorFrame").GetComponent<AudioSource>();

		sBox = GameObject.Find ("SpeechBox");
		sText = GameObject.Find ("SpeechText").GetComponent<Text> ();

		sBox.SetActive (false);
		choose3.SetActive(false);
		credits.SetActive (false);

		allIngrediants["u"].AClip = candyClip;
		allIngrediants["h"].AClip = catClip;
		allIngrediants["n"].AClip = loveClip;
		allIngrediants["y"].AClip = fruitClip;
		allIngrediants["b"].AClip = spaceClip;
		allIngrediants["g"].AClip = bananaClip;
		allIngrediants["r"].AClip = lemonClip;

	}
	
	// Update is called once per frame
	void Update () {
		if (gameStarted && characterStatus == 0 && currCharacter < allChars.Count) {
			characterStatus = 1;
			StartCoroutine (EnterCharacter ());
		} else if (currCharacter >= allChars.Count) {
			//TODO: SHOW CREDITS THEN RESTART GAME
			credits.SetActive (true);
		}

		if (Input.GetMouseButtonDown(0)) {
			NextLine ();
		}

		// Check if its time to choose
		if (characterStatus == 2) {

			foreach (string key in allIngrediants.Keys) {
				if (Input.GetKey (key) && chosenIgs.Contains(key) != true) {
					chosenIgs.Add (key);
					ChooseIg(key);
				}
				if (chosenIgs.Count >= 3) {
					StartCoroutine (MixIngrediants ());
				}
			}

		}
	}

	IEnumerator EnterCharacter() {
		loader.LoadLobby ();

		if (currCharacter > 0) {
			int oldCharNum = currCharacter - 1;
			GameObject.Find ("Char" + oldCharNum.ToString()).SetActive (false);
		}

		yield return new WaitForSeconds(1.5f);
		yield return StartCoroutine(OpenDoors());

		GameObject newChar = GameObject.Find ("Char" + currCharacter.ToString ());
		currCharSource = newChar.GetComponent<AudioSource> ();
		float y = 0f;
		float zResult = 1.5f;
		if (currCharacter == 4) {
			y = 1.4f;
			zResult = 1f;
		}
		newChar.transform.position = new Vector3 (newChar.transform.position.x, y, newChar.transform.position.z);

		while (newChar.transform.position.z > cam.transform.position.z + zResult) {
			newChar.transform.position = new Vector3(newChar.transform.position.x, newChar.transform.position.y, newChar.transform.position.z - .2f);
			yield return null;
		}

		yield return StartCoroutine(CloseDoors());

		currLines = allChars[currCharacter].enterDialog;
		ShowSpeech ();
	}

	IEnumerator ExitCharacter() {
		yield return new WaitForSeconds(.5f);

		rSource.Stop ();
		rSource.clip = eDing;
		rSource.Play ();

		yield return new WaitForSeconds(.5f);
		yield return StartCoroutine(OpenDoors());
		yield return StartCoroutine(sp.PlaySymbols(new string[]{ allIngrediants[chosenIgs[0]].Name, allIngrediants[chosenIgs[1]].Name, allIngrediants[chosenIgs[2]].Name }));
		yield return new WaitForSeconds(.5f);
		if (chosenIgs.Count > 0) {
			CalcResponse ();
		}

		GameObject newChar = GameObject.Find ("Char" + currCharacter.ToString()); 
		while (newChar.transform.position.z < cam.transform.position.z + 10f) {
			newChar.transform.position = new Vector3(newChar.transform.position.x, newChar.transform.position.y, newChar.transform.position.z + .2f);
			yield return null;
		}
	}

	
	void CalcResponse() {
		currLines = new List<string> (){};

		//FOR NOW ONE MATCH MAKE SENSE GIVEN THEIR SUCCESS/FAIL MESSAGES
		foreach (string igKey in chosenIgs) {
			string igName = allIngrediants[igKey].Name;
			foreach (string fav in allChars[currCharacter].favKeys) {
				if (fav == igName) {
					currLines = new List<string>() { allChars[currCharacter].Success };
					//return formattedResponse; 
					break;
				}
			}
			if (currLines.Count > 0) {
				break;
			}
		}

		if (currLines.Count <= 0) {
			currLines = new List<string>() { allChars[currCharacter].Fail };
		}

		currLine = 0;
		ShowSpeech ();
		
		chosenIgs.Clear ();
	}

	IEnumerator ReturnLobby() {
		yield return new WaitForSeconds(2f);
		yield return StartCoroutine(CloseDoors());

		foreach (GameObject i in icons) {
			i.transform.position = new Vector3(10, 10, 10);
		}

		yield return new WaitForSeconds(1f);

		rSource.Stop ();
		rSource.clip = eDown;
		rSource.Play ();

		yield return new WaitForSeconds (1f);

		lSource.Stop ();
		lSource.clip = lobbyClip;
		lSource.Play ();

		yield return new WaitForSeconds(1f);

		characterStatus = 0;
		currCharacter = currCharacter + 1;
	}

	IEnumerator OpenDoors() {
		eSource.Stop ();
		eSource.clip = eOpen;
		eSource.Play ();

		while (doorL.transform.position.x > doorLClosed.x - 2) {
			doorL.transform.position = new Vector3(doorL.transform.position.x - elevatorSpeed, doorL.transform.position.y, doorL.transform.position.z);
			doorR.transform.position = new Vector3(doorR.transform.position.x + elevatorSpeed, doorR.transform.position.y, doorR.transform.position.z);
			yield return null;
		}
		yield return null;
	}

	IEnumerator CloseDoors() {
		eSource.Stop ();
		eSource.clip = eClose;
		eSource.Play ();

		while (true) {
			doorL.transform.position = new Vector3(doorL.transform.position.x + elevatorSpeed, doorL.transform.position.y, doorL.transform.position.z);
			doorR.transform.position = new Vector3(doorR.transform.position.x - elevatorSpeed, doorR.transform.position.y, doorR.transform.position.z);
			if (doorL.transform.position.x > doorLClosed.x) {
				doorL.transform.position = doorLClosed;
				doorR.transform.position = doorRClosed;
				break;
			}
			yield return null;
		}
		yield return null;
	}

	void ChooseIg(string key) {
		GameObject icon = GameObject.Find("Icon" + allIngrediants[key].Name);
		float offset = (chosenIgs.Count - 1) * .24f - .25f;
		icon.transform.position = new Vector3(panel.transform.position.x + offset, panel.transform.position.y -.06f , panel.transform.position.z - .1f);
	}
	
	IEnumerator MixIngrediants() {
		choose3.SetActive(false);

		List<string> chosenNames = new List<string>();
		AudioClip levelSound = null;
		bool hasSound = false;

		for (int i = 0; i < chosenIgs.Count; i += 1) {
			string ig = chosenIgs[i];
			chosenNames.Add (allIngrediants[ig].Name);
			if (allIngrediants[ig].HasSound) {
				levelSound = allIngrediants[ig].AClip;
				hasSound = true;
			}
			//GameObject icon = GameObject.Find("Icon" + allIngrediants[ig].Name);
			//float offset = i * .24f - .25f;
			//icon.transform.position = new Vector3(panel.transform.position.x + offset, panel.transform.position.y -.06f , panel.transform.position.z - .1f);
		}

		sBox.SetActive (false);
		characterStatus = 3;

		rSource.Stop ();
		rSource.clip = eUp;
		rSource.Play ();

		loader.LoadLevel (chosenNames);

		yield return new WaitForSeconds(2.5f);

		if (hasSound) {
			lSource.Stop ();
			lSource.clip = levelSound;
			lSource.Play ();
		}
		StartCoroutine (ExitCharacter());
	}

	void ShowSpeech () {
		if (currLine == 0) {
			sBox.SetActive(true);
			currCharSource.Play ();
		}

		sText.text = currLines[currLine];
		currLine = currLine + 1;
	}

	void NextLine() {
		if (currLine > 0) {
			if (currLine >= currLines.Count) {
				if (currLine == currLines.Count) {
					if (characterStatus == 1) {
						characterStatus = 2;
						sBox.SetActive(false);
						choose3.SetActive(true);
						currCharSource.Stop ();
					}
				}

				currLine = 0;

				if (characterStatus > 2) {
					sBox.SetActive(false);
					currCharSource.Stop ();
				}


				if (characterStatus == 3) {
					characterStatus = 4;
					StartCoroutine(ReturnLobby ());
				}

			} else {
				ShowSpeech ();
			}
		}
	}
}
